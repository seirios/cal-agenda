# cal-agenda

Modified version of cal.c from util-linux-2.32-rc1

Adds support for reading a file and using it as an agenda.

File has the following format on each line:
day month year description

Each field separated by whitespace as per scanf %s.
Description field may contain spaces, break is at newline.

Supports multiple events per day.

Events are shown after every row of months, corresponding to those months.

All days with events are highlighted on a terminal that supports it (instead of just the current date).

Also weekends are in bold for better visualization.

Agenda is enabled with -a <file> or --agenda <file>.

Enjoy!
